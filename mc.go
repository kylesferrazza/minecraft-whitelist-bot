package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"

	"github.com/willroberts/minecraft-client"
)

var serverIP string
var rconPassword string

func init() {
	var ipSet, pwSet bool

	serverIP, ipSet = os.LookupEnv("MC_SERVER_IP")
	if !ipSet {
		log.Fatalln("MC_SERVER_IP was not set.")
	}

	rconPassword, pwSet = os.LookupEnv("MC_RCON_PW")
	if !pwSet {
		log.Fatalln("MC_RCON_PW was not set.")
	}
}

func setupMcClient() *minecraft.Client {
	client, err := minecraft.NewClient(serverIP)
	if err != nil {
		fatal("%s", err)
	}

	if err := client.Authenticate(rconPassword); err != nil {
		fatal("%s", err)
	}
	return client
}

func addToWhitelist(client *minecraft.Client, username string) (string, error) {
	resp, err := client.SendCommand("whitelist add " + username)
	if err != nil {
		log.Printf("Error adding to whitelist: %s", err)
		return "", err
	}
	log.Printf("%s", resp.Body)
	return resp.Body, nil
}

var numOnlineRegex = regexp.MustCompile("There are ([0-9+]) of a max")

func getStatus(client *minecraft.Client) string {
	num, err := getNumOnline(client)
	if err != nil {
		log.Printf("Error getting num players: %s", err)
		return "with error handling"
	}
	return fmt.Sprintf("Minecraft: %d online", num)
}

func getNumOnline(client *minecraft.Client) (int64, error) {
	resp, err := client.SendCommand("list")
	if err != nil {
		return 0, err
	}
	matches := numOnlineRegex.FindStringSubmatch(resp.Body)
	if len(matches) != 2 {
		return 0, errors.New("regex did not match")
	}
	intVal, err := strconv.ParseInt(matches[1], 10, 32)
	if err != nil {
		return 0, err
	}
	return intVal, nil
}
