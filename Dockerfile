FROM golang:1.17-alpine AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN go build -o /minecraft-whitelist-bot



FROM alpine:latest
WORKDIR /
COPY --from=build /minecraft-whitelist-bot /minecraft-whitelist-bot
ENTRYPOINT ["/minecraft-whitelist-bot"]
