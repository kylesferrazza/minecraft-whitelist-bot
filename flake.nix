{
  description = "mc-whitelist-bot";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.buildGoModule {
      name = "mc-whitelist-bot";
      version = "0.1";

      src = ./.;
      vendorSha256 = "e4kfwAYYZYcc2ccpcTkJnvktNfQH+CWWZtg33dxgUH8=";
    };
    devShell.x86_64-linux = pkgs.mkShell {
      name = "mc-whitelist-bot";
      buildInputs = with pkgs; [
        go
        gopls
      ];
    };
  };
}
