module gitlab.com/kylesferrazza/mc-whitelist-bot

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.3-0.20210529215543-f5bb723db8d9
	github.com/willroberts/minecraft-client v1.1.0
)
