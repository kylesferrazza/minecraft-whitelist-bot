package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"regexp"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/willroberts/minecraft-client"
)

var botToken string
var mcClient *minecraft.Client
var discordSession *discordgo.Session

var MCUsernameRegex = regexp.MustCompile("^[a-zA-Z0-9_]*$")

func init() {
	var tokenSet bool
	botToken, tokenSet = os.LookupEnv("DISCORD_TOKEN")
	if !tokenSet {
		log.Fatalln("DISCORD_TOKEN was not set.")
	}

}

var whitelistCommand = discordgo.ApplicationCommand{
	Name:        "whitelist",
	Description: "Whitelist users on the minecraft server",
	Options: []*discordgo.ApplicationCommandOption{
		{
			Type:        discordgo.ApplicationCommandOptionString,
			Name:        "username",
			Description: "The minecraft username to add to the whitelist",
			Required:    true,
		},
	},
}

func validUsername(test string) bool {
	return MCUsernameRegex.Match([]byte(test))
}

func handleWhitelist(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if i.Data.Name != "whitelist" {
		return
	}

	username := i.Data.Options[0].StringValue()
	isValid := validUsername(username)
	if !isValid {
		s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionApplicationCommandResponseData{
				Content: "Invalid username!",
			},
		})
		return
	}
	res, err := addToWhitelist(mcClient, username)
	var content string
	if err != nil {
		content = fmt.Sprintf("Error sending minecraft command: %s", err)
	} else {
		content = res
	}
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionApplicationCommandResponseData{
			Content: content,
		},
	})
}

func shutdown() {
	mcClient.Close()
	discordSession.Close()
}

func fatal(format string, v ...interface{}) {
	shutdown()
	log.Fatalf(format, v...)
}

func updateStatus() {
	status := getStatus(mcClient)
	discordSession.UpdateGameStatus(0, status)
}

func statusTimer() {
	for {
		time.Sleep(10 * time.Second)
		go updateStatus()
	}
}

func main() {
	mcClient = setupMcClient()
	var err error
	discordSession, err = discordgo.New("Bot " + botToken)
	if err != nil {
		fatal("Invalid bot parameters: %v", err)
	}
	discordSession.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		log.Println("Bot is up!")
	})

	discordSession.AddHandler(handleWhitelist)
	err = discordSession.Open()
	if err != nil {
		fatal("Cannot open the session: %v", err)
	}
	cmds, err := discordSession.ApplicationCommands(discordSession.State.User.ID, "")
	if err != nil {
		fatal("Cannot retrieve commands: %v", err)
	}
	for _, cmd := range cmds {
		err = discordSession.ApplicationCommandDelete(discordSession.State.User.ID, "", cmd.ID)
		if err != nil {
			fatal("Cannot delete command (%s): %v", cmd.Name, err)
		}
		log.Printf("Deleted command: %s", cmd.Name)
	}
	cmd, err := discordSession.ApplicationCommandCreate(discordSession.State.User.ID, "", &whitelistCommand)
	if err != nil {
		fatal("Cannot create command: %v", err)
	}
	log.Printf("Created command: %s", cmd.Name)

	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)

	go statusTimer()

	<-stop
	log.Println("Gracefully shutting down...")
	shutdown()
}
